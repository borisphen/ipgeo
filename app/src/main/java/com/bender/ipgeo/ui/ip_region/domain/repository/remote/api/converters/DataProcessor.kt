package com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.converters

interface DataProcessor<I, O>: Converter<I, O> {
    fun save(input: O)
}