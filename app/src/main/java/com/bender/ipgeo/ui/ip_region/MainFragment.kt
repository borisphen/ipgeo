package com.bender.ipgeo.ui.ip_region

import android.os.Bundle
import android.widget.Toast
import com.adc.muse.ui.BaseFragment
import com.bender.ipgeo.R
import com.bender.ipgeo.databinding.FragmentMainBinding
import com.bender.ipgeo.utils.IpUtils
import org.koin.android.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment<FragmentMainBinding>() {
    private val mainViewModel: MainViewModel by viewModel()

    override fun getLayoutId() = R.layout.fragment_main

    override fun init(savedInstanceState: Bundle?) {
        dataBinding?.viewmodel = mainViewModel
        mainViewModel.let {
            it.errorLiveData.observe(viewLifecycleOwner, { error ->
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
            })
            it.locationData.observe(viewLifecycleOwner, { data ->
                Toast.makeText(requireContext(), data, Toast.LENGTH_LONG).show()
            })
        }
        dataBinding?.edtIp!!.filters = arrayOf(IpUtils.ipCheckFilter())
    }
}