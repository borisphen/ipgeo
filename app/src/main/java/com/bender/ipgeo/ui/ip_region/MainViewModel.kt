package com.bender.ipgeo.ui.ip_region

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.adc.muse.ui.BaseViewModel
import com.bender.ipgeo.GeoApp
import com.bender.ipgeo.R
import com.bender.ipgeo.ui.ip_region.domain.location.LocationUseCase
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.Result
import com.bender.ipgeo.utils.IpUtils

class MainViewModel(private val locationUseCase: LocationUseCase) : BaseViewModel() {

    val ipAddress = MutableLiveData<String>()
    val locationData = MutableLiveData<String>()

    private fun getLocationByIp(ip: String) {
        doAsync {
            when (val result = locationUseCase.getLocationByIp(ip)) {
                is Result.Success -> {
                    locationData.postValue(result.data.toString())
                }
                is Result.Error -> {
                    errorLiveData.postValue(result.text)
                }
            }
        }
    }

    fun setSelection(selector: View) {
        when (selector.id) {
            R.id.btnGetData -> validateAndSend()
        }
    }

    private fun validateAndSend() {
        val enteredIp = ipAddress.value
        if (isIpvValid(enteredIp)) {
            getLocationByIp(enteredIp!!)
        } else {
            errorLiveData.value = GeoApp.instance.getString(R.string.error_format)
        }
    }

    private fun isIpvValid(ip: String?): Boolean {
        ip?.let {
            return IpUtils.isIpValid(ip)
        }
        return false
    }

}