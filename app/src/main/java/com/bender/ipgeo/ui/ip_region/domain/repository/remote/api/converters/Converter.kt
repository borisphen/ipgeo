package com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.converters

interface Converter<I, O> {
    fun convert(input: I): O
}