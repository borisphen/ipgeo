package com.bender.ipgeo.ui.ip_region.domain

import com.bender.ipgeo.ui.ip_region.domain.repository.remote.Result
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.Api
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.ErrorData
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.converters.DataProcessor
import com.bender.ipgeo.utils.ErrorUtils
import retrofit2.Response
import retrofit2.Retrofit

abstract class BaseRepositoryProvider(
    open val api: Api,
    open val retrofit: Retrofit,
) {

    companion object {
        const val API_STATUS_SUCCESS = "success"
    }

    /**
     * Invoke REST request to Backend and convert result to local DB data type
     *
     * @param T - data type returned by http(s) request
     * @param O - data type after converting (mainly local DB data type)
     * @param call - rest api invocation
     * @param dataProcessor - object for converting remote objects to local DB objects
     * @return Result.Success with local DB object, or Result.Error with error message
     */
    suspend fun <T : Any, O : Any> apiCallAndSave(
        call: suspend () -> Response<T>,
        dataProcessor: DataProcessor<T, O>
    ): Result<O> {
        return try {
            val response = call.invoke()
            return if (response.isSuccessful) {
                val localEntity = dataProcessor.convert(input = response.body()!!)
                dataProcessor.save(localEntity)
                Result.Success(localEntity)
            } else {
                ErrorUtils.processErrors(retrofit, response)
            }
        } catch (e: Exception) {
            ErrorUtils.processException(e)
        }
    }

    /**
     * Invoke REST request to Backend and returns result without converting
     *
     * @param T - data type returned by http(s) request
     * @param call - rest api invocation
     * @return Result.Success with remote object, or Result.Error with error message
     */
    suspend fun <T : Any> apiCall(call: suspend () -> Response<T>): Result<T> {
         try {
            val response = call.invoke()
            if (response.isSuccessful) {
                val data = response.body()!! as ErrorData
                if (API_STATUS_SUCCESS == data.status) {
                    return Result.Success(response.body()!!)
                }
                return Result.Error(data.message!!)
            } else {
                return ErrorUtils.processErrors(retrofit, response)
            }
        } catch (e: Exception) {
            return ErrorUtils.processException(e)
        }
    }
}