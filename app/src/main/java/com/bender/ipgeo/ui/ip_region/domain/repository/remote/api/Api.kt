package com.bender.ipgeo.ui.ip_region.domain.repository.remote.api

import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.location.LocationData
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("/json/{query}")
    fun getLocationByIpAsync(@Path("query") query: String): Deferred<Response<LocationData>>

}