package com.bender.ipgeo.ui.ip_region.domain.repository.remote.api

open class ErrorData {
   val message: String? = null
   val query: String = ""
   val status: String = ""
}