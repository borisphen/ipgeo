package com.bender.ipgeo.ui.ip_region.domain.location

import com.bender.ipgeo.ui.ip_region.domain.BaseRepositoryProvider
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.Result
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.Api
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.location.LocationData
import retrofit2.Retrofit

class LocationRepositoryProvider(
    override val api: Api,
    override val retrofit: Retrofit,
) : BaseRepositoryProvider(
    api, retrofit
), LocationRepository {
    override suspend fun getLocationByIp(query: String): Result<LocationData> {
        return apiCall(
            call = { api.getLocationByIpAsync(query).await() }
        )
    }
}