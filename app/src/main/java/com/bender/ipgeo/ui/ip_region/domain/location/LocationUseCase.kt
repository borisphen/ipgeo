package com.bender.ipgeo.ui.ip_region.domain.location

import com.bender.ipgeo.ui.ip_region.domain.repository.remote.Result
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.location.LocationData

interface LocationUseCase {
    suspend fun getLocationByIp(query: String): Result<LocationData>
}