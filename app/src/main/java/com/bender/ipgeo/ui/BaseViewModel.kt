package com.adc.muse.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import timber.log.Timber

abstract class BaseViewModel : ViewModel() {
    val errorLiveData = MutableLiveData<String>()
    private val dispatcher = Dispatchers.IO

    private val handler = CoroutineExceptionHandler { _, exception ->
        Timber.e("Caught! $exception")
    }

    // Do async operation bounded to ViewModel lifecycle
    fun doAsync(function: suspend () -> Unit) {
        viewModelScope.launch {
            withContext(dispatcher) {
                function.invoke()
            }
        }
    }

    // Do async operation bounded to Application lifecycle
    fun doAsyncGlobal(function: suspend () -> Unit) {
        GlobalScope.launch(handler) {
            withContext(dispatcher) {
                function.invoke()
            }
        }
    }
}