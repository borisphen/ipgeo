package com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.location

import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.ErrorData
import com.squareup.moshi.Json

data class LocationData(
    val country: String,
    val countryCode: String,
    val region: String,
    val regionName: String,
    val city: String,
    val zip: String,
    val lat: Double,
    val lon: Double,
    val timezone: String,
    val isp: String,
    val org: String,
    @Json(name = "as")
    val dataAs: String
): ErrorData()
