package com.bender.ipgeo.ui.ip_region.domain.location

import com.bender.ipgeo.ui.ip_region.domain.repository.remote.Result
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.location.LocationData

/**
 * Implements business logic
 *
 * Just connection between ViewModel and Repository in this case (no extra logic)
 */
class LocationInteractor(private val repository: LocationRepository): LocationUseCase {
    override suspend fun getLocationByIp(query: String): Result<LocationData> = repository.getLocationByIp(query)
}