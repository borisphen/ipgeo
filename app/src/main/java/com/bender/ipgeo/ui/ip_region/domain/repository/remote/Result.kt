package com.bender.ipgeo.ui.ip_region.domain.repository.remote

/**
 * @author Boris Rayskiy
 *
 * Created on Jun 2019
 */
sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val text: String, val code: Int = 200) : Result<Nothing>()
}