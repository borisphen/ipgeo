package com.bender.ipgeo

import android.app.Application
import com.bender.ipgeo.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class GeoApp : Application() {
    companion object {
        lateinit var instance: GeoApp
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        init()
    }

    private fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            // use AndroidLogger as Koin Logger - default Level.INFO
            androidLogger()
            // use the Android context given there
            androidContext(this@GeoApp)
            // load properties from assets/koin.properties file
            androidFileProperties()
            // module list
            modules(mainModule)
        }
    }

    fun isDevEnvironment(): Boolean = BuildConfig.BUILD_TYPE == "debug"
}