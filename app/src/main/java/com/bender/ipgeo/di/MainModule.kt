package com.bender.ipgeo.di

import com.bender.ipgeo.BuildConfig
import com.bender.ipgeo.ui.ip_region.MainViewModel
import com.bender.ipgeo.ui.ip_region.domain.location.LocationInteractor
import com.bender.ipgeo.ui.ip_region.domain.location.LocationRepository
import com.bender.ipgeo.ui.ip_region.domain.location.LocationRepositoryProvider
import com.bender.ipgeo.ui.ip_region.domain.location.LocationUseCase
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.Api
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

const val BASE_URL = "http://ip-api.com/"
const val TIME_OUT_SEC: Long = 60


val mainModule = module {
    single<OkHttpClient> { provideDefaultOkHttpClient() }
    single<Retrofit> { provideRetrofit(get()) }
    single<Api> { provideApi(get()) }
    single<LocationRepository> { provideLocationRepository(get(), get()) }
    single<LocationUseCase> { provideLocationUseCase(get()) }

    viewModel { MainViewModel(get()) }
}

fun provideDefaultOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    // set log level based on build type
    logging.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
    else HttpLoggingInterceptor.Level.NONE
    return OkHttpClient.Builder()
        .addInterceptor(logging)
        .readTimeout(TIME_OUT_SEC, TimeUnit.SECONDS)
        .writeTimeout(TIME_OUT_SEC, TimeUnit.SECONDS)
        .connectTimeout(TIME_OUT_SEC, TimeUnit.SECONDS)
        .build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}

fun provideApi(retrofit: Retrofit) = retrofit.create(Api::class.java) as Api

fun provideLocationRepository(api: Api, retrofit: Retrofit) =
    LocationRepositoryProvider(api, retrofit)

fun provideLocationUseCase(locationRepository: LocationRepository) = LocationInteractor(locationRepository)