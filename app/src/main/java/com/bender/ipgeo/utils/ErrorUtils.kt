package com.bender.ipgeo.utils

import com.bender.ipgeo.GeoApp
import com.bender.ipgeo.R
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.Result
import com.bender.ipgeo.ui.ip_region.domain.repository.remote.api.ErrorData
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.net.UnknownHostException

/**
 *
 * Manages errors from BE
 */
object ErrorUtils {

    private val genericErrorMessage: String = GeoApp.instance.getString(R.string.error_generic)

    /**
     * Converts not successful response to Result.Error
     *
     * @param retrofit - Retrofit instance
     * @param response - not successful response
     * @return - Result.Error
     */
    fun processErrors(retrofit: Retrofit, response: Response<*>): Result.Error {
        parseError(retrofit, response).let { errorData ->
            return try {
                Result.Error(getErrorMessage(errorData), response.code())
            } catch (exception: Exception) {
                processException(exception)
            }
        }
    }

    /**
     * Parse unsuccessful response
     *
     * @param retrofit - Retrofit instance
     * @param response - not successful response
     * @return - parsed error data
     */
    private fun parseError(retrofit: Retrofit, response: Response<*>): ErrorData? {
        val converter = retrofit
            .responseBodyConverter<ErrorData>(ErrorData::class.java, arrayOfNulls(0))

        var error: ErrorData? = null

        try {
            error = converter.convert(response.errorBody()!!)
        } catch (e: IOException) {
        } finally {
            return error
        }
    }

    /**
     * Transforms exception to Result.Error
     *
     * @param e - exception
     * @return - Result.Error
     */
    fun processException(e: Exception): Result.Error {
        return when (e) {
            is UnknownHostException -> {
                Result.Error(genericErrorMessage)
            }
            is HttpException -> {
                when (e.code()) {
                    // Customize Http errors if needed
                    400, 401 -> Result.Error(e.message(), e.code())
                    500 -> Result.Error(genericErrorMessage, e.code())
                    else -> Result.Error(e.localizedMessage!!, e.code())
                }
            }
            else -> {
                // Generic exception
                Result.Error(genericErrorMessage)
            }
        }
    }

    /**
     * Converts error from server to defined locally
     *
     * @param errorData - data from BE
     * @return - Error message to show to user
     */
    private fun getErrorMessage(errorData: ErrorData?): String {
        errorData?.let {
            return errorData.message ?: genericErrorMessage
        }
        return genericErrorMessage
    }
}
