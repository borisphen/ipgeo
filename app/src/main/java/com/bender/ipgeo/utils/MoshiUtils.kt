package com.bender.ipgeo.utils

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi

object MoshiUtils {

    /**
     * Converts data to JSON string
     *
     * @param T - data type
     * @param data - data instance
     * @return - json string
     */
    inline fun <reified T : Any> toJson(data: T): String {
        val moshi = Moshi.Builder().build()
        val jsonAdapter: JsonAdapter<T> = moshi.adapter<T>(data.javaClass)
        return jsonAdapter.toJson(data)
    }

    /**
     * Converts JSON string to data
     *
     * @param T - data type
     * @param jsonString - converted data as JSON string
     * @return - Data object
     */
    inline fun <reified T> fromJson(jsonString: String?): T? {
        if (jsonString.isNullOrBlank()) return null
        val moshi = Moshi.Builder().build()
        val adapter = moshi.adapter<T>(T::class.java)
        return adapter.fromJson(jsonString)
    }
}