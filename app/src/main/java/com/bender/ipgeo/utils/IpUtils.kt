package com.bender.ipgeo.utils

import android.text.InputFilter
import android.util.Patterns


object IpUtils {
    fun ipCheckFilter(): InputFilter {
        return InputFilter { source, start, end, dest, dstart, dend ->
            if (end > start) {
                val destTxt = dest.toString()
                val resultingTxt = destTxt.substring(0, dstart) +
                        source.subSequence(start, end) +
                        destTxt.substring(dend)
                if (!resultingTxt.matches(
                        Regex(
                            "^\\d{1,3}(\\." +
                                    "(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?"
                        )
                    )
                ) {
                    return@InputFilter ""
                } else {
                    val splits = resultingTxt.split(".").toTypedArray()
                    for (i in splits.indices) {
                        if (splits[i] == "") continue
                        if (splits[i].toInt() > 255) {
                            return@InputFilter ""
                        }
                    }
                }
            }
            null
        }
    }

    fun isIpValid(ip: String): Boolean = Patterns.IP_ADDRESS.matcher(ip).matches()
}