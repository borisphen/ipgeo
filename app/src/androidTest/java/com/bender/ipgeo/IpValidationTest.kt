package com.bender.ipgeo

import com.bender.ipgeo.utils.IpUtils.isIpValid
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
class IpValidationTest {

    private val IP_CORRECT = "192.168.0.1"
    private val IP_WRONG_SECTION = "192.168.256.1"
    private val IP_WRONG_LENGTH = "192.168."
    private val IP_WRONG_DELIMITERS = "192,168,0,1"

    @Test
    fun validateIpTest() {
        Assert.assertTrue(isIpValid(IP_CORRECT))
        Assert.assertFalse(isIpValid(IP_WRONG_SECTION))
        Assert.assertFalse(isIpValid(IP_WRONG_LENGTH))
        Assert.assertFalse(isIpValid(IP_WRONG_DELIMITERS))
    }

}