package com.bender.ipgeo

import com.bender.ipgeo.di.mainModule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.koinApplication
import org.koin.test.KoinTest
import org.koin.test.check.checkModules

@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
class ModuleTest : KoinTest {
    /**
     * Tests dependency injection
     *
     */
    @Test
    fun checkDefinitions() {
        koinApplication {
            androidContext(GeoApp.instance)
            modules(mainModule)
        }.checkModules()
    }
}
